execute pathogen#infect()
syntax enable
set background=dark
colorscheme solarized

" Fix slow yaml
au BufNewFile,BufRead *.yaml,*.yml so ~/.vim/yaml.vim

" Leader Shortcuts
let mapleader = ","
nnoremap <leader>u :GundoToggle<CR>     " Visual undo
noremap <Leader>s :update<CR>           " Quick save
noremap <leader>n :NERDTreeToggle<CR>

" Toggle line numbers
noremap <leader>m :set invnumber<CR>
inoremap <leader>m :set invnumber<CR>


" Spaces and tabs
set tabstop=4  		" number of visual spaces per TAB
set softtabstop=4	" number of spaces in tab when editing
set expandtab 		" tabs are spaces

" Searching
set incsearch           " search as characters are entered
set hlsearch            " highlight matches
" turn off search highlight

nnoremap <leader><space> :nohlsearch<CR>

" ui config
set number              " show line numbers
set showcmd             " show command in bottom bar
set cursorline          " highlight current line
set wildmenu            " visual autocomplete for command menu
set lazyredraw          " redraw only when we need to.
set showmatch           " highlight matching [{()}]

" Movement
" move vertically by visual line
nnoremap j gj
nnoremap k gk
" highlight last inserted text
nnoremap gV `[v`]

" allows cursor change in tmux mode
if exists('$TMUX')
        let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
        let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
else
        let &t_SI = "\<Esc>]50;CursorShape=1\x7"
        let &t_EI = "\<Esc>]50;CursorShape=0\x7"
endif

set pastetoggle=<F2> " paste toggle

" moves the backup dir to tmp 
set backup
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set writebackup

" folding
set foldenable          " enable folding
set foldlevelstart=10   " open most folds by default
set foldnestmax=10      " 10 nested fold max
set foldmethod=indent   " fold based on indent level
" space open/closes folds
nnoremap <space> za

" keeps .netrwlist out of vim
let g:netrw_home=$XDG_CACHE_HOME.'/vim'

" Syntax Checker
" Disable for python
let g:syntastic_python_python_exec = '/usr/loca/bin/python'
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
